# Quick howto (unix)
1. `chmod +x run runTests`
2. `./runTests`
3. `./run docs/*`

# Quick howto (win)
1. Make sure javac is in the Path
2. `runTestsWin`
3. `runWin docs\*`
