package findwiseAssignment;
import java.util.*;

import findwiseAssignment.Util.ScoredDoc;
import findwiseAssignment.Util.DocWithFreq;



class Scoring {
    /*
      Sort by score
     */
    static class ScoreComparator implements Comparator<ScoredDoc> {
	public int compare (ScoredDoc d1,
			    ScoredDoc d2) {
	    // Possible truncation errors
	    return (int) (1000*(d2.score - d1.score));
	}
    }
    static ScoreComparator comp = new ScoreComparator();
    
    public static List<ScoredDoc> applyTfIdf (SearchIndex db,List<DocWithFreq> docList, String term) {
	List<ScoredDoc> scoredDocs
	    = new LinkedList<ScoredDoc>();
	
	for (DocWithFreq df : docList){
	    ScoredDoc sd = new ScoredDoc
		(df.docIndex,df.freq,TfIdf(db,term,df.docIndex));
	    scoredDocs
		.add(sd);
	    
	}
	return scoredDocs;
    }
    public static void sortByScore (List<ScoredDoc> scoredDocs) {
	Collections.sort(scoredDocs,comp);
    }

    /*
      Calculate Scoring
    */
    public static Double TfIdf(SearchIndex db,String term,Integer docIndex){
	double normTermF = normalizedTermFreq(db,term,docIndex);
	double invF = invFreq(db,term,docIndex);
	assert(normTermF>0.0);
	return normTermF * invF;
    }
    public static Double normalizedTermFreq(SearchIndex db,String term,Integer docIndex){
	Document d = db.getDoc(docIndex);
	return (double)rawFreq(db,term,docIndex) / d.maxFreq();
    }
    public static Integer rawFreq(SearchIndex db,String term,Integer docIndex){
	Document d = db.getDoc(docIndex);
	return d.getTerms().get(term);
    }
    public static Double invFreq(SearchIndex db,String term,Integer docIndex){
	int nrD = db.nrDocs();
	int nrD2 = db.nrOfDocsContaining(term);
	assert(nrD>0);
	return Math.log((double)nrD / (double)nrD2);
    }
}