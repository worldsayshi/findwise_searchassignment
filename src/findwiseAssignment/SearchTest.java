package findwiseAssignment; 

import java.util.*;
import java.io.IOException;

import findwiseAssignment.SearchIndex;
import findwiseAssignment.Util.ScoredDoc;

class SearchTest {
    private SearchIndex db;

    public static void main(String[] args){
	try {
	    SearchTest test = new SearchTest();
	    test.setUp();
	    test.simpleTests();
	    test.runExamples();
	} catch (IOException e){
	    Util.p("IO error: "+ e.toString());
	}
    }
    
    private void setUp() throws IOException {
	String[] filePaths = {"docs/doc1","docs/doc2","docs/doc3"};
	db = new SearchIndex();
	db.getDocumentsFromPaths(filePaths);
	db.createIndex();
    }

    /*
      A few sanity tests
     */
    private void simpleTests() {
	assert(db.getMatches("").size()==0);
	check(db.getMatches("foobarbazbaxster").size()==0);
    }

    /*
      Examples according to assignment
     */
    private void runExamples() {
	
	List<ScoredDoc> scoredResult 
	    = db.request("brown");

	assert(db.getDoc(scoredResult.get(0).docIndex).path=="docs/doc1");
	assert(db.getDoc(scoredResult.get(1).docIndex).path=="docs/doc2");

	scoredResult 
	    = db.request("fox");

	assert(db.getDoc(scoredResult.get(0).docIndex).path=="docs/doc1");
	assert(db.getDoc(scoredResult.get(1).docIndex).path=="docs/doc3");
    }


    /*
      a soft assertion
     */
    private void check(boolean cond){
	if(!cond){
	    Util.p("Warning: check failed at "+getLineNumber(2));
	}
    }

    // source: 
    // http://stackoverflow.com/questions/115008/how-can-we-print-line-numbers-to-the-log-in-java
    public static int getLineNumber(int offset) {
	try {
	    offset += 2;
	    return Thread.currentThread()
		.getStackTrace()[offset].getLineNumber();
	} catch (IndexOutOfBoundsException e) {
	    return -1;
	}
    }
}