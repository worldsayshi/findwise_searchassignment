package findwiseAssignment;




class Util {
    /*
    static class Tuple2<A,B> {
	public final A _1;
	public final B _2;
	public Tuple2(A a,B b){
	    this._1=a;
	    this._2=b;
	}
    }
    static class Tuple3<T_1,T_2,T_3> {
	public final T_1 _1;
	public final T_2 _2;
	public final T_3 _3;
	public Tuple3(T_1 _1,T_2 _2,T_3 _3){
	    this._1=_1;
	    this._2=_2;
	    this._3=_3;
	}
    }*/

    static class DocWithFreq {
	final int docIndex; final int freq;
	public DocWithFreq(int docIndex, int freq) {
	    this.docIndex=docIndex; this.freq=freq;
	}
    }

    static class ScoredDoc {
	final int docIndex; final int freq; final double score;
	public ScoredDoc (int docIndex,int freq,double score) {
	    this.docIndex=docIndex; this.freq=freq;this.score=score;
	}
    }

    public static void p(String msg){
	System.out.println(msg);
    }
    public static void p(String[] l){
	for(int i=0;i<l.length;i++){
	    p(l[i]);
	}
    }
}