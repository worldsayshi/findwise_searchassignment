package findwiseAssignment; 
import java.util.*;
import java.util.Map.Entry;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

import findwiseAssignment.Util.ScoredDoc;
import findwiseAssignment.Util.DocWithFreq;
import findwiseAssignment.Scoring;

class SearchIndex {
    private HashMap<String,List<DocWithFreq>> revIndex;
    private List<Document> docs;
    private HashMap<String,Integer> globalWordFreq;

    public SearchIndex() {
	globalWordFreq = new HashMap<String,Integer>();
	revIndex = new HashMap<String,List<DocWithFreq>>();
    }

    // Retrieve a scored and sorted list of documents 
    // given a search term
    public List<ScoredDoc> request(String term) {
	List<DocWithFreq> result 
	    = getMatches(term);
	
	List<ScoredDoc> scoredResult
	    = Scoring.applyTfIdf(this,result,term);
	Scoring.sortByScore(scoredResult);
	return scoredResult;
    }

    // Get document by index
    public Document getDoc(int index) {
	return docs.get(index);
    }
    // Find documents matching a term
    public List<DocWithFreq> getMatches(String term) {
	List<DocWithFreq> matches = revIndex.get(term);
	if(matches==null){
	    return new LinkedList<DocWithFreq>();
	}
	return revIndex.get(term);
    }

    // nr of documents in index
    public int nrDocs() {
	return docs.size();
    }

    // Count the number of documents containing a
    // certain term
    public Integer nrOfDocsContaining(String term){
	int count = 0;
	for(Document d : docs){
	    if(d.getTermFreq(term)>0){
		count+=1;
	    }
	}
	return count;
    }

    // Map a term to a document in the reverse index
    public void revIndexPut(String term,DocWithFreq df) {
	List<DocWithFreq> entry 
	    = revIndex.get(term);
	if(entry==null){
	    entry = new LinkedList<DocWithFreq>();
	}
	entry.add(df);
	revIndex.put(term,entry);
    }

    // Import documents from files
    public List<Document> getDocumentsFromPaths (String[] paths) 
	throws IOException {
	List<Document> docs = new ArrayList<Document>();

	for (int i=0;i<paths.length;i++){	    
	    BufferedReader inp
		= new BufferedReader(new FileReader(paths[i]));
	    
	    String line, content = "";
	    while((line = inp.readLine())!=null){
		if(content!=""){
		    content+=" ";
		}
		content += line;
	    }
	    docs.add(new Document(paths[i],content));
	}
	this.docs=docs;

	return docs;
    }

    // Construct reverse index from
    // documents
    public void createIndex() {
	for (int i=0;i<nrDocs();i++) {
	    HashMap<String,Integer> tokensWithFreq 
		= tokenize(getDoc(i));
	    mapTokens(i,tokensWithFreq);
	}
    }

    // Tokenize a document;
    // extract and count its terms
    private HashMap<String,Integer> tokenize (Document doc) {
	String content = doc.getContent();
	String[] naiveSet = content.split(" ");
	HashMap<String,Integer> localFreqs
	    = new HashMap<String,Integer>();
	for (int i=0;i<naiveSet.length;i++) {
	    String token = naiveSet[i];
	    Integer freq = localFreqs.get(token);
	    freq=(freq==null)?
		1:freq+1;
	    localFreqs.put(token,freq);
	}
	return localFreqs;
    }

    // Map all terms/tokens of a document
    // into the reverse index and count 
    // global term occurrence
    private void mapTokens(Integer docIndex, 
		     HashMap<String,Integer> tokensWithFreq) {
	Set<String> tokens = tokensWithFreq.keySet();
	getDoc(docIndex).setTerms(tokensWithFreq);
	for(String token : tokens){
	    int freq = tokensWithFreq.get(token);
	    incGlobFreq(token,freq);
	    DocWithFreq df
		= new DocWithFreq(docIndex,freq);
	    revIndexPut(token, df);
	}
    }

    // Add a (document) specific term frequecy to the 
    // global count
    private void incGlobFreq (String token, int freq) {
	Integer currFreq = globalWordFreq.get(token);
	Integer newFreq = (currFreq==null)?
	    freq:currFreq+freq;
	globalWordFreq.put(token,newFreq);
    }
    
    // Print out the reverse index
    public void printIndex() {
	for(Entry<String,List<DocWithFreq>> e : revIndex.entrySet()){
	    String s = "{";
	    String term = e.getKey();
	    s+="term: "+term+", "+
		"globalFreq: "+globalWordFreq.get(term)+", "+
		"matches: [";

	    List<DocWithFreq> docTuples = e.getValue();
	    for(DocWithFreq df : docTuples){
		s+="{docIndex: "+df.docIndex+", "+
		    "termFreq: "+df.freq+"}, ";
	    }
	    s+="]}";
	    Util.p(s);
	}
    }
}