package findwiseAssignment; 

import java.util.*;
import java.util.Map.Entry;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import findwiseAssignment.Document;
import findwiseAssignment.SearchApp;
import findwiseAssignment.SearchIndex;
import findwiseAssignment.Util.ScoredDoc;
import findwiseAssignment.Util.DocWithFreq;


public class SearchApp {
    

    private SearchIndex db;

    public SearchApp (String[] filePaths) 
	throws IOException {
	db = new SearchIndex();
	db.getDocumentsFromPaths(filePaths);
	db.createIndex();	
    }

    public static void main(String[] args){
	String[] filePaths = args;
	
	try{
	    SearchApp app = new SearchApp(filePaths);

	    Util.p("\n*** Documents ***");
	    app.printDocs();
	    
	    Util.p("\n*** Index ***");
	    app.printIndex();

	    app.searchDialogue();
	}
	catch(IOException e){
	    Util.p("IO error: "+ e.toString());
	}
    }
    
    // console interaction
    public void searchDialogue() throws IOException {
	BufferedReader inp 
	    = new BufferedReader(new InputStreamReader(System.in));
	while(true){
	    Util.p("Enter a single search term:");
	    String query = inp.readLine();
	    Util.p("q:"+query);
	    String[] qTerms = query.split(" ");
	    if(qTerms.length>1){
		Util.p("Warning, only single terms are supported. "+
		  "Only the first term will be used.");
	    }
	    String term = qTerms[0];
	    List<ScoredDoc> scoredResult
		= db.request(term);

	    Util.p("*** Results: ***");
	    printScored(scoredResult);
	}
    }

    /* 
       Print various data structures
     */

    public void printResult(List<DocWithFreq> result) {
	for(DocWithFreq df : result){
	    Document d = db.getDoc(df.docIndex);
	    Util.p(d.toString());
	}
    }
    public void printScored(List<ScoredDoc> result) {
	for(ScoredDoc sDoc : result){
	    Document doc = db.getDoc(sDoc.docIndex);
	    Util.p("["+sDoc.score+"]->"+doc.toString());
	}
    }
    public void printIndex() {
	db.printIndex();
    }
    public void printDocs() {
	for(int i=0;i<db.nrDocs();i++){
	    Util.p(db.getDoc(i).toString());
	}
    }
}