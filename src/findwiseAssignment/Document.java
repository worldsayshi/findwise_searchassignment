package findwiseAssignment;
import java.util.*;

public class Document{
    final public String path;
    private String content;
    // frequency of terms in the document
    private HashMap<String,Integer> termsWithFreq;
    public Document(String path, String content) {
	this.path=path;
	this.content=content;
    }
    public String getContent() {
	return content;
    }
    public void setTerms (HashMap<String,Integer> termsWithFreq) {
	this.termsWithFreq=termsWithFreq;
    }
    public HashMap<String,Integer> getTerms () {
	return this.termsWithFreq;
    }
    @Override
    public String toString () {
	return "{"+"path: "+path+", "+"content: "+content+"}";
    }
    // get frequency of specific term
    public Integer getTermFreq(String term){
	Integer f = termsWithFreq.get(term);
	if(f==null){
	    return 0;
	}
	return f;
    }
    // frequency of the most frequent 
    // term
    public Integer maxFreq() {
	Integer m = 0;
	for(Integer f : getTerms().values()){
	    m = f>m ? f:m;
	}
	return m;
    }
}
